# Setup Raspberry Pi with Ubuntu Core

## Requirements

[Help](https://ubuntu.com/download/raspberry-pi-core)

Make sure you have Ubuntu One account with an ssh public key imported in it.
You need to remember user id and password of the Ubuntu One account.

## Setup

Prepare SD card:

```sh
sudo snap install rpi-imager
```

Run the app and select 'Ubuntu Core 32-bit armh' or 'Ubuntu Core 20 64-bit IoT OS for arm64'


