# Setup nodejs development environment in Multipass using Ansible

## Clone the repository

```
git clone https://gitlab.com/tabrez/dominus.git
```

## Change defaults

Change to project folder:

```sh
cd dominus/webdev
```

+ Open `files/cloud-config.yml` file in a text editor 
+ (Optional) Replace `tabrez` with your preferred user name
  (in 4 locations; do search & replace)
+ Replace `ssh_authorized_keys` value with your public ssh key
  (e.g. ~/.ssh/id_rsa.pub)
+ (Optional) Open `mp-create.sh` file in a text editor and modify vm parameters
  (e.g. cpus, mem, disk, etc.)

## Create vm

Run the following command in `webdev` folder

```sh
sh mp-create.sh
```

## Misc commands

+ Get the IP address of the newly created vm:

  ```sh
  multipass info webdev | grep IPv4
  ```

+ Completely delete the VM:

  ```sh
  multipass delete --purge webdev
  ```

+ Access shell using ssh

  ```sh
  ssh user@ip
  ```

+ Access shell for ubuntu user(for troubleshooting purposes)

  ```sh
  multipass shell webdev
  ```

## Copy private ssh key (Optional)

SSH key pair setup inside the vm is necessary if you need to push projects
from inside the vm to Github, etc.

It's recommended that you generate a new pair of SSH keys inside the VM and
then upload the public key to Github, etc. But if you like to copy existing
private ssh key from your local machine to the multipass vm, run the following
command:

```sh
# chmod 400 ~/.ssh/id_rsa
scp ~/.ssh/id_rsa user@ip:~/.ssh/id_rsa
```

