# Nodejs Dev Box

## Quickstart for nodejs dev env inside Multipass VM

[Quickstart](Quickstart.md)

## Create a container or virtual machine

### Multipass

+ Install Multipass

+ Create `cloud-config.yaml` file
  Example cloud-init config file: https://gist.github.com/tabrez/37af739ca71f7623f94f8e20c132cc49
  Download the file and paste your public key in it:

  ```sh
  wget -O cloud-config.yaml https://gist.githubusercontent.com/tabrez/37af739ca71f7623f94f8e20c132cc49/raw/daac5656f1eb9f81f0822b0d1f90471487141a10/cloud-init.yaml
  cat ~/.ssh/id_rsa.pub | xclip -sel c
  ```

  Or customise `../files/cloud-config.yaml`(used by default by this project);
  at minimum you should replace the public ssh key in it

+ Create a new VM and get its ip address

  ```sh
  multipass launch -n webdev --cloud-init ../files/cloud-config.yaml
  multipass info webdev | grep IPv4
  ```

  Replace example ip address in `hosts` file with the above ip address(`mp_node` section)

+ Provision the vm using ansible

  ```sh
  ansible-playbook -i hosts mp-playbook.yml
  ```

+ Access the shell using multipass command

  ```sh
  multipass shell webdev
  ```

+ Access the shell using SSH

  ```sh
  # username as per `cloud-config.yaml` file, ip address as per `multipass info webdev`
  ssh user@ipaddress 
  ```

+ Access the VM using VS Code

  - Install VS Code
  - Install `Remote - SSH` or `Remote Development` extension
  - Run `Add New SSH Host...` and enter the details
  - Run `Connect to Host...` and select the host configured above
  - Click the `Open Directory` button and select a directory(create it from the shell as necessary)

+ SSH config

  If you get any SSH errors, try to use the ssh config file from `../files/ssh_config`

  ```sh
  mkdir ~/.ssh
  cp ../files/ssh_config ~/.ssh/config
  ```

### Proxmox LXC

+ Create a Proxmox LXC instance and get its ip address

Make sure you have vault password in `vault_password` file in `~/.ansible` directory
and the following config in `~/.ansible.cfg`:

```ansible.cfg
[defaults]
roles_path=~/.ansible/roles:/home/tabrez/MEGA/devops/current/ansible-roles
vault_password_file=~/.ansible/vault_password
```

Generate a file called `credentials` with appropriate passwords, look at `../files/credentials.example` for help on format of the file

```sh
ansible-vault create credentials
```

Edit values in `hosts` file, `vars` section in `infra-lxc.yml` file appropriately. 
Also take a look at values in `defaults/main.yml` file in `infra-lxc` role.

```sh
ap -i hosts infra-lxc.yml --tags create-lxc
```

+ Replace ip address in `hosts` file(`lxc_node` section) with the ip address of above
lxc instance

+ Provision the instance using ansible

```sh
ansible-playbook -i hosts lxc-playbook.yml
```

