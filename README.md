# Ansible roles & playbooks to configure proxmox lxc and multipass instances

## Using this repo for development purpose

+ Clone the repository: `git clone git@gitlab.com:tabrez/dominus.git`
+ Copy ansible config file to home folder, adjust rolesr_path in it if needed: `cp ansible.cfg ~/.ansible.cfg`
+ Set vault password file: `echo <password> > /tmp/vault_pass`
+ Change to the directory of the project you want, edit `playbook.yml` with the ip address of the machine
to configure(e.g. localhost)
+ Run the playbook with a command like this: `ansible-playbook playbook.yml`

## Desktop projects

+ `desktop` project configures local ubuntu 22.04 machine: [More info](desktop/README.md)
+ `desktop-vm` project configures ubuntu 22.04 vm: [More info](desktop-vm/README.md)
+ `headless-vm` project configures ubuntu 22.04 vm without GUI: [More info](headless-vm/README.md)

## NodeJS

+ `webdev` project configures proxmox lxc container or multipass vm with
nodejs development environment: [More info](webdev/README.md)
+ `webdev-mp` project is an easier start to configuring multipass vm with
nodejs development environment: [More info](webdev-mp/README.md)

## Proxmox

Remaining projects configure proxmox lxc instances with apps & services like
nfs, samba, qBittorrent, plex, etc. Refer to README.md of respective folders.

