# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

EDITOR=vi
VISUAL=vi

# Control bash history
# http://www.biostat.jhsph.edu/~afisher/ComputingClub/webfiles/KasperHansenPres/IntermediateUnix.pdf
# https://unix.stackexchange.com/questions/48713/how-can-i-remove-duplicates-in-my-bash-history-preserving-order
# don't put duplicate lines or lines starting with space in the history.
export HISTCONTROL=ignoreboth:erasedups
# big big history size
export HISTSIZE=100000
# set history format to include timestamps
HISTTIMEFORMAT="%Y-%m-%d %T "

# save & restore bash history in all tmux panes/windows immediately
PROMPT_COMMAND="history -a; history -c; history -r"

# when the shell exits, append to the history file instead of overwriting it
shopt -s histappend
# save all lines of a multiple-line command in the same history entry
shopt -s cmdhist
# check the window size after each command and, if necessary, update the values of LINES and COLUMNS.
shopt -s checkwinsize
# correct simple errors while using cd
shopt -s cdspell

# Auto-complete command from history using up and down arrow keys
# http://lindesk.com/2009/04/customize-terminal-configuration-setting-bash-cli-power-user/
bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'
bind '"\C-p": history-search-backward'
bind '"\C-n": history-search-forward'

# Change command prompt
export PS1="\[\e[38;5;35m\][\h:\[\e[38;5;33m\]\W\[\e[38;5;35m\]]\n➜ \[$(tput sgr0)\]"

if [ -f ~/.aliases ]; then
   . ~/.aliases
fi

export PATH=$PATH:~/.local/bin:~/bin

# neovim
if [ -d ~/.local/bin/nvim/bin ]; then
  export PATH=$PATH:~/.local/bin/nvim/bin
fi

# distrobox
if [ -d ~/.local/bin/distrobox ]; then
  export PATH=$PATH:~/.local/bin/distrobox/bin
fi

# fnm
if [ -d ~/.local/fnm ]; then
  export PATH=~/.local/fnm:$PATH
fi

# powerprofilesctl set performance

type zoxide >/dev/null 2>&1 && eval "$(zoxide init bash)"
type direnv >/dev/null 2>&1 && eval "$(direnv hook bash)"

if [ -z "$TMUX" ]
then
    tu
fi

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8

