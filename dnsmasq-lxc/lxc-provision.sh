#!/bin/bash

# convert this script to an ansible playbook using include_tasks or include_roles magic if possible

ansible-playbook -i hosts lxc-create.yml --tags create-lxc
sleep 5
ansible-playbook -i hosts lxc-playbook.yml

