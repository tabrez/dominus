# Setup configuration files, command-line utilities and apps on Kubuntu 23.10

## Manual steps

+ Configure `SDDM` -> `Behaviour` to login user with `Wayland` session and reboot/logout
+ Configure display settings, taskbar and alt-tab behaviour
+ Select `Start with an empty session` in `Desktop Session`
+ Create new terminal profile, select JetBrains Mono font and increase the font size
+ Copy/clone dominus folder and change directory to it
+ Install ansible from its ppa for ubuntu 23.10/23.04: `bash ansible_install.sh`
+ Edit `playbook.yml` if needed and then run the following commands:

```sh
echo <password> > /tmp/vault_pass
cp .ansible.cfg ~/
ansible-playbook playbook.yml
```

+ Copy Firefox and Thunderbird profile folders to their respective paths
+ Add the following apps to auto-start list in Kubuntu: `Telegram`, `Discord`, `Thunderbird`
+ Set blank KWallet password so that it uses user's password: `KWallet Manager` -> `Change password`
+ Ask `ksshaskpass` to remember the ssh key; you can change this passphrase from KWalletManager
+ Add keybind to suspend Kubuntu: `Shortcuts` -> `Power Management` -> `Suspend`
+ Map CAPSLOCK to ESC and SHIFT+CAPSLOCK to CAPSLOCK: `Keyboard` -> `Advanced`
+ Sleep when power button is pressed: `Energy Saving`

