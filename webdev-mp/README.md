# Setup nodejs development environment in Multipass using Ansible

## Install Multipass

[Help](https://multipass.run/install)

## Clone the repository

```sh
git clone https://gitlab.com/tabrez/dominus.git
```

## Change defaults

Change to project folder:

```sh
cd dominus/webdev-mp
```

+ Open `cloud-config.yml` file in a text editor
+ Replace `ssh_authorized_keys` value with your public ssh key
  (e.g. ~/.ssh/id_rsa.pub)
+ (Optional) Replace `shaper` with your preferred user name
  (in all locations; do search & replace)
+ (Optional) Open `mp-create.sh` file in a text editor and modify vm parameters
  (e.g. cpus, mem, disk, etc.)

## Create VM

Run the following command in `webdev-mp` folder

```sh
sh mp-create.sh
```

## Change git user settings

Edit `~/.gitconfig` with correct values in '[user]' section

## Get IP address of the VM

```sh
multipass info webdev | grep IPv4
```

## Access the VM using VS Code

+ Install VS Code
+ Install `Remote - SSH` or `Remote Development` extension
+ Run `Add New SSH Host...` and enter the details; `ssh user@ip-adderss` should work
+ Run `Connect to Host...` and select the host configured above
+ Click the `Open Directory` button and select a directory
(create it from the terminal as necessary)

## SSH config

If you get any SSH errors, try to use the provided config file:

```sh
# mkdir ~/.ssh
cp ssh_config ~/.ssh/config
```

## Misc commands

+ Completely delete the VM:

  ```sh
  multipass delete --purge webdev
  ```

+ Access shell using ssh

  ```sh
  ssh user@ip
  ```

+ Access shell for ubuntu user(for troubleshooting purposes)

  ```sh
  multipass shell webdev
  ```

[Multipass Docs](https://multipass.run/docs/create-an-instance)


## Copy private ssh key (Optional)

SSH key pair setup inside the vm is necessary if you need to push projects
from inside the vm to Github, etc.

It's recommended that you generate a new pair of SSH keys inside the VM and
then upload the public key to Github, etc. But if you like to copy existing
private ssh key from your local machine to the multipass vm, run the following
command:

```sh
# chmod 400 ~/.ssh/id_rsa
scp ~/.ssh/id_rsa user@ip:~/.ssh/id_rsa
```

## What is available

+ ssh server is installed
+ modern command line utilities are installed
+ basic bash, git, vim, tmux, ssh configuration files are copied
+ nvm, nodejs and some global npm packages are installed
+ vscode extensions are installed
+ `tldr` command is available to check quick documentation on any linux command

See `dominus\desktop` project for a full list of command line utilities, global npm
packages, and vscode extensions that are installed.

