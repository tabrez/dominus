#!/bin/bash

# Define an array of roles
roles=(
    # "ansible",
    # "init",
    # "user",
    # "copy-private-ssh-key", 
    # "copy-public-ssh-key",
    # "password-less-sudo",
    # "ssh-config",
    # "config-local",
    # "utils",
    # "debs"
    # "neovim",
    "lazyvim",
    # "astrovim",
    # "docker",
    # "multipass",
    # "setup-root",
    # "mamba-fastai-env",
    # "ssh-server",
    # "packer",
    # "terraform",
    # "vscode-extensions",
    # "megacmd",
    # "nfs-fstab"
    # "smb-fstab"
)

# Build the tags string by joining the roles with a comma
tags=$(IFS=,; echo "${roles[*]}")

# Run the ansible-playbook command with the specified tags
ansible-playbook -i ini playbook.yml --tags "$tags" -e password=$6$tyEhGsCl$4jtxYYOfIoVRe.TEX0NFlkOZY7piq65LNy6W9NVCE.gBdcw3K.9XnvxZcWG4fuIj7mElnCzd41N29yCkL4uW51
