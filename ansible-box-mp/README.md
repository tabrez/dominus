# Basic setup for Ubuntu 22.04 system

## How to use

Either install Ansible manually or using cloud-init config file.

Clone this repository and provision using Ansible

```sh
git clone https://gitlab.com/tabrez/dominus.git
cd dominus/ansible-box-mp
cp ../ansible.cfg ~/.ansible.cfg
echo "<vault_password>" > /tmp/vault_password
ansible-playbook playbook.yml
```

## What is available

+ private and public ssh keys are setup in ~/.ssh
+ user is setup to work without sudo password

Following command-line utilities are installed:

+ trash-cli (alternative to `rm`; available as `tr` alias)
+ xclip & xsel (copy to clipboard)
+ fzf (fuzzy file selector)
+ fd-find (modern replacement for find; installed as `fdfind` binary and `fd` alias)
+ rip-grep (modern replacement for grep; installed as `rg`)
+ neofetch (show system info in terminal)
+ ranger (terminal file manager)
+ bat (modern replacement for cat; installed as `batcat` binary and `bat` alias)
+ ncdu (shows disk usage interactively)
+ tmux (available as `tu` alias)
+ exa (modern replacement for ls; available as `k` alias)

+ duf (modern replacement for `df`)
+ dust (modern replacement for `du`)
+ zoxide (similar to `z`, `autojump`, `fasd`)
+ tldr (modern replacement for man pages)
+ cheat (modern replacement for man pages)
+ bottom (modern replacement for htop)
+ httpie (modern replacement for curl; available as `http` & `https`)
+ direnv (allows exporting environment variables automatically)
+ task (simplify long commands using task files)

+ neovim and astrovim 
+ vscode extensions are installed in case you connect with VS Code remotely via FTP

You can google any of the above package/command names to learn more
about them(add 'github' to your search expression for easier results).
Check `~/.aliases` file to know if they have any aliases
defined.

Following config is copied:

+ bashrc
+ aliases
+ vimrc
+ gitconfig
+ tmux.conf
 
