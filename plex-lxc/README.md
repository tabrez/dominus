# Create an LXC container in Proxmox using Ansible

## Helpful resources

[Ansible proxmox module](https://docs.ansible.com/ansible/latest/collections/community/general/proxmox_module.html)
[Example 1](https://github.com/UdelaRInterior/ansible-role-proxmox-create-lxc)
[Example 2](https://blog.dmcindoe.dev/posts/2021-07-31/automating-proxmox-with-ansible/)
