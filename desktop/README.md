# Basic setup for Ubuntu 22.04 system

## How to use

Clone this repository and provision using Ansible

```sh
git clone https://gitlab.com/tabrez/dominus.git
cd dominus
cp ansible.cfg ~/.ansible.cfg
echo "<vault_password>" > /tmp/vault_pass
sudo apt install -y ansible
## for newer version of ansible:
# bash ansible_install.sh
ansible-playbook desktop/playbook.yml
```

## What is available

Check `main.yml` files in various sub-directories of `roles\` directory

+ openssh-server is installed so we can ssh into this machine
+ virt-viewer is installed so we can access desktops of other machines remotely
+ vs code is installed as a deb package
  - A `settings.json` file is copied and some extensions are installed
+ Mullvad is installed as a deb package.
  - Enable access to local network in settings(so 127.0.0.1, 192.168.0.1, etc. addresses are accessible)
  - Open a second browser from `Split Tunnelling` option to bypass VPN for Hotstar, Twitch.tv etc.
+ Vivaldi web browser is installed to run Hotstar, Twitch, or Youtube websites bypassing VPN

Following command-line utilities are installed:

+ trash-cli (alternative to `rm`; available as `tr` alias)
+ xclip & xsel (copy to clipboard)
+ fzf (fuzzy file selector)
+ fd-find (modern replacement for find; installed as `fdfind` binary and `fd` alias)
+ rip-grep (modern replacement for grep; installed as `rg`)
+ neofetch (show system info in terminal)
+ ranger (terminal file manager)
+ bat (modern replacement for cat; installed as `batcat` binary and `bat` alias)
+ ncdu (shows disk usage interactively)
+ tmux (available as `tu` alias)
+ exa (modern replacement for ls; available as `k` alias)

+ duf (modern replacement for `df`)
+ dust (modern replacement for `du`)
+ zoxide (similar to `z`, `autojump`, `fasd`)
+ tldr (modern replacement for man pages)
+ cheat (modern replacement for man pages)
+ bottom (modern replacement for htop)
+ httpie (modern replacement for curl; available as `http` & `https`)
+ direnv (allows exporting environment variables automatically)
+ task (simplify long commands using task files)

You can google any of the above package/command names to learn more
about them(add 'github' to your search expression for easier results).
Check `~/.aliases` file to know if they have any aliases
defined.

Following config is copied:

+ bashrc
+ aliases
+ vimrc
+ gitconfig
+ tmux.conf
+ vs code settings.json
 
Following Flatpak apps are installed:

+ Mozilla Thunderbird
+ Telegram
+ Discord
+ Obsidian
+ MEGA Sync
+ Transmission

Following snap apps are installed:

+ Multipass

docker & docker compose are installed. docker compose is installed as a plugin; run
the command as `docker compose up -d` and not as `docker-compose up -d`

Distrobox is installed using a bash script

Dock has the following pinned: Terminal, Firefox, VS Code, Obsidian in that order.

Caps Lock key is mapped to Escape key

Firefox & Thunderbird profile folders can be copied from MEGAsync

+ Run the playbook with the following tags:

```sh
ansible-playbook playbook.yml \
    --skip-tags mozilla-profiles \
    --skip-tags configure-ssh \
    -K
```

+ Run MEGAsync and configure shared folders(map to folder ~/MEGA on local
machine)

+ Run the tasks using the following tags:

```sh
ansible-playbook playbook.yml --tags mozilla-profiles --tags configure-ssh -K
```

+ You might have to manually select the copied profiles:

```sh
flatpak run org.mozilla.Thunderbird -P # select default-release
```

## Autostart

Following apps are configured to start automatically:

+ Telegram
+ Discord
+ Thunderbird
+ MEGAsync
+ Mullvad

## What is not available

+ You need to install ansible manually:

```sh
apt-add-repository -y ppa:ansible/ansible
apt-get install -y ansible
```

+ zsh, emacs, etc. are not installed
+ You need to set the resolution & scaling manually 
+ You need to copy the ssh keys manually

Following minimal config files are downloaded in `config-web` role but
currently not used in the playbook:

+ bashrc
+ aliases
+ gitconfig
+ vimrc

Instead the versions of these config files in `files/` folder are being used.

## TODO

+ Simplify how to copy mozilla profile folders & ssh keys
+ Split `utils` role
+ add basic, usable neovim config just like `.vimrc`
+ add a role to install ansible which can be useful when provisioning a remote
machine
+ install `lunarvim` or `astrovim` in distrobox or multipass using ansible
