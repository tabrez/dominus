# Provision Samba server in Ubuntu 22.04 server using Ansible

Configure a machine/VM/container for file sharing using Samba server.

## Requirements

+ A non-root user is recommended(maybe required). Change the username in
`vars` section of `playbook.yml`
+ IP address of the machine and ssh access through that IP address(password or ssh key)
+ Debian based system(tested only on Ubuntu 22.04)

## Provision

+ Edit `hosts` file with correct ip address & username under [samba_server] section
+ Edit `vars` section of `playbook.yml` with correct username & preferred shared
directory path
+ (Optional: skip this step if you already have a machine/vm/container to install
samba server on)
Create `vars.yml` file in root directory with passwords in json format:

```json
{
  proxmox_password:12345
  lxc_password: 12345 
}
```

Enter correct proxmox_password and pick any thing you want for lxc_password.

Take a loot at `defaults/main.yml` file of `create-lxc` role if you want to override
any other variables, you can do this in `playbook.yml` or a var file.

Run the following command to create an lxc instance in proxmos

```sh
ap -i hosts infra-lxc.yml -K --tags create-lxc
```

Enter sudo password of proxmox host when prompted

+ Run the following command to provision Samba server:

```sh
ap -i hosts provision-samba.yml -K
```

Enter sudo password of ubuntu server when prompted(`lxc_password` if you used above
step to create lxc instance)

## Mount shared directory on local machine

Install `cifs-utils` package, create a local directory and mount Samba share on it

```sh
sudo apt install -y cifs-utils
mkdir ~/data
sudo mount -t cifs -o guest,file_mode=0644,dir_mode=0755,uid=1000,gid=1000 //192.168.0.63/smb ~/smb
```

+ Without `uid` & `gid` values, the local folder will have root as owner and non-root
users won't have write permissions to the shared directory
+ Without `file_mode` & `dir_mode` values matching the permissions set in samba config
file, newly created files & directories from local machine will not match the
permissions of the same files on server machine
+ Without `guest` option, mount command might prompt for user credentials

Add the following line to `etc/fstab` if you want to auto-mount the Samba shared folder
on boot:

```sh
//192.168.0.63/smb /home/<user>/smb cifs guest,uid=1000,gid=1000 0 0
```

Replace ip address with the correct one

## Troubleshooting

### Check if Samba server is running on the server machine

```sh
sudo systemctl status smbd
# sudo systemctl start smbd
```

### Check if the shared directory(`data` by default) is shown in the list
using `smbclient`

On server machine:

```sh
sudo apt install -y smblcient
# `tldr smbclient` if you have `tldr` installed
man smbclient
smbclient -N -L localhost
```

On client machine:
```sh
sudo apt install -y smblcient
smbclient -N -L 192.168.0.63
```

Check if the directory is currently being shared with/mounted on any machine

```sh
sudo smbstatus --shares
```

### Using GNOME file manager

Open GNOME file manager and go to `Locations` in left sidebar, enter 
`smb://192.168.0.63/data` in `Connect to server` textbox at the bottom of the
window and click `Connect`, try to connect as anonymous user.

## Resources

+ How to configure Samba to export public shared directory
[Samba config](https://www.techrepublic.com/article/how-to-create-a-passwordless-guest-share-in-samba/)

+ How to mount anonymous/guest/public shared Samba directory on client machine
[Help](https://linuxconfig.org/how-to-mount-a-samba-shared-directory-at-boot)

+ Use this tutorial to debug using GNOME file manager
[Ubuntu official docs](https://ubuntu.com/tutorials/install-and-configure-samba#4-setting-up-user-accounts-and-connecting-to-share)

+ Working file permissions
[Stackoverflow](https://unix.stackexchange.com/questions/103415/why-are-files-in-a-smbfs-mounted-share-created-with-executable-bit-set)

