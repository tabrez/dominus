#!/bin/bash

# mamba() {
#     docker run --rm condaforge/mambaforge:latest mamba "$@"
# }
# shopt -s expand_aliases
# alias mamba='docker run -it --rm condaforge/mambaforge:latest mamba'
mamba init
mamba create -n fastbook -y
mamba activate fastbook
mamba install --yes -c conda-forge ipywidgets sentencepiece nbdev \
  jupyterlab:4.0.4 jupyterlab-autosave-on-focus-change \
  jupyterlab_execute_time
mamba install --yes -c fastchan -c pytorch -c nvidia \
  fastbook pytorch torchvision torchaudio pytorch-cuda=11.7
jupyter labextension disable "@jupyterlab/apputils-extension:announcements"
md "{{ home }}/mambaforge/envs/fastbook/share/jupyter/lab/settings"
cp overrides.json "{{ home }}/mambaforge/envs/fastbook/share/jupyter/lab/settings/overrides.json"
