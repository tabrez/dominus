# RancherOS in Proxmox VM

## Install

[official](http://rancher-com-website-main-elb-elb-1798790864.us-west-2.elb.amazonaws.com/docs/os/v1.0/en/running-rancheros/server/install-to-disk/)
[linuxhint](https://linuxhint.com/install_rancher_os/)

## Connect

```sh
ssh rancher@192.168.0.65
```

## Tips

NFS mount:

```sh
sudo mount -t nfs -o "nolock,proto=tcp,addr=192.168.0.60" 192.168.0.60:/nfs ~/nfs
```

Or:

```yaml
#cloud-config
mounts:
- ["192.168.1.109:/mnt/data", "/mnt/test", "nfs4", "nolock,proto=tcp,addr=192.168.1.109"]
```

## Containers

+ qBitTorrent
+ plex/jellyfin
+ filebot
+ snippetbox
+ docker registry
+ portainer

## Provision

ansible-playbook -i hosts playbook.yml

